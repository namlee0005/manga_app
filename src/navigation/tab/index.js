import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeStack from '../stack/HomeStack'
import ProfileStack from '../stack/ProfileStack'
import React, { Component } from 'react'
import { Styles, Media } from '../../config/Ultils'
import { StyleSheet, View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const Tab = createBottomTabNavigator();

const BottomComponent = ({ iconName, title, focused }) => {
    let colorTitle = focused ? '#3D6AF2' : '#CBCBCB'
    return (
        <View style={Styles.center}>
            <Icon name={iconName} color={colorTitle} size={20}/>
            <Text style={{ textAlign: 'center', color: colorTitle, fontSize: 10 }}>{title}</Text>
        </View>
    )
}

const MainTab = () => {
    return (
        <Tab.Navigator
            headerMode={'none'}
            tabBarOptions={{
                showLabel: false
            }}
        >
            <Tab.Screen
                name="HomeTab"
                component={HomeStack}
                options={({ navigation }) => {
                    return ({
                        tabBarIcon: ({ focused, color, size }) => {
                            return (
                                <BottomComponent iconName={'ios-home'} title={'Trang chủ'} focused={focused} />
                            )

                        },
                    })
                }}
            />
            <Tab.Screen name="SuperMarketTab" component={HomeStack}
                options={({ navigation }) => {
                    return ({
                        tabBarIcon: ({ focused, color, size }) => {
                            return (
                                <BottomComponent iconName={'logo-buffer'} title={'Phân loại'} focused={focused} />
                            )
                        },

                    })
                }}
            />
            <Tab.Screen name="WalletTab" component={HomeStack}
                options={({ navigation }) => {
                    return ({
                        tabBarIcon: ({ focused, color, size }) => {
                            return (
                                <BottomComponent iconName={'md-compass'} title={'Khám phá'} focused={focused} />
                            )
                        },

                    })
                }}
            />
            <Tab.Screen name="NotificationTab" component={HomeStack}
                options={({ navigation }) => {
                    return ({
                        tabBarIcon: ({ focused, color, size }) => {
                            return (
                                <BottomComponent iconName={'md-bookmarks'} title={'Tủ sách'} focused={focused} />
                            )
                        },

                    })
                }}
            />
            <Tab.Screen name="ProfileTab" component={ProfileStack}
                options={({ navigation }) => {
                    return ({
                        tabBarIcon: ({ focused, color, size }) => {
                            return (
                                <BottomComponent iconName={'md-book'} title={'Cá nhân'} focused={focused} />
                            )
                        },

                    })
                }}
            />
        </Tab.Navigator>
    );
}

export default MainTab;

const styles = StyleSheet.create({
    imageView: {
        height: 20,
        aspectRatio: 1,
        overflow: 'hidden',
    },
});

