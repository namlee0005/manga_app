import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack'
import MainTab from '../tab'
import React from 'react'

const Stack = createStackNavigator();

const MainDrawer = () => {
    return (
        <Stack.Navigator headerMode={null} >
            <Stack.Screen
                name="HomeDrawer"
                component={MainTab}
                
            />
        </Stack.Navigator>
    )
}

export default MainDrawer;