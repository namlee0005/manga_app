import { combineReducers } from 'redux';
import AppReducer from './reducer';

export default combineReducers({
    AppReducer
});