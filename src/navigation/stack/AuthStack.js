import { createStackNavigator } from '@react-navigation/stack';
import Login from '../../views/Auth/Login';
import SignUp from '../../views/Auth/SignUp';
import React from 'react';
const Stack = createStackNavigator();

const AuthStack = () => {
    return (
        <Stack.Navigator headerMode={'none'}>
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="SignUp" component={SignUp} />
        </Stack.Navigator>
    );
}

export default AuthStack;
