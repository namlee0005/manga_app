import {CONST} from '../constant';
import AsyncStorage from '@react-native-community/async-storage'

export const httpService = {
    get: async (url) => {
        const token = await AsyncStorage.getItem(CONST.TOKEN);
        const options = {
            method: 'get',
            headers: {
                Accept: 'Application/json',
                "Content-type": "Application/json",
                Authorization: "Bearer " + token
            }
        }
        return fetch(url, options).then(res => res.json())
    },
    getWithoutToken: async (url) => {
        const options = {
            method: 'get',
            headers: {
                Accept: 'Application/json',
                "Content-type": "Application/json",
            }
        }
        return fetch(url, options).then(res => res.json())
    },
    post: async (url, data) => {
        const token = await AsyncStorage.getItem(CONST.TOKEN);
        const options = {
            method: 'post',
            headers: {
                Accept: 'Application/json',
                "Content-type": "Application/json",
                Authorization: "Bearer " + token
            },
            body: JSON.stringify(data)
        }
        return fetch(url, options).then(res => res.json())
    },
    postWithoutToken: async (url, data) => {
        const options = {
            method: 'post',
            headers: {
                Accept: 'Application/json',
                "Content-type": "Application/json",
            },
            body: JSON.stringify(data)
        }
        return fetch(url, options).then(res => res.json())
    },

    uploadFile: async (url, data) => {
        let options = {
            method: 'POST',
            body: data
        };
        return fetch(url, options).then(res => res.json())
    }
};
