import { createStackNavigator } from '@react-navigation/stack';
import Profile from '../../views/Profile/Profile'
import React from 'react';
const Stack = createStackNavigator();

const DiscoverStack = () => {
    return (
        <Stack.Navigator headerMode={'none'}>
            <Stack.Screen name="Profile" component={Profile} />
        </Stack.Navigator>
    );
}

export default DiscoverStack;
