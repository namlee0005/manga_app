import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import Global from '../Global';
import FastImage from 'react-native-fast-image'

export const Button = ({title, onPress, styleButton, styleTitle, iconLeft, iconRight, styleIconLeft, styleIconRight, ...rest}) => (
    <TouchableOpacity
        {...rest}
        style={[styles.button, styleButton]}
        onPress={() => {
            if (onPress) onPress()
        }}
    >
        {iconLeft && <FastImage source={iconLeft} style={[styles.imageLeft, styleIconLeft]}/>}
        <Text style={[styles.title, styleTitle]}>{title}</Text>
        {iconRight && <FastImage source={iconRight} style={[styles.imageRight, styleIconRight]}/>}
    </TouchableOpacity>
);

const styles = {
    button: {width: Global.ScreenWidth - 32, height: 40, marginVertical: 16, justifyContent: 'center', alignItems: 'center', borderRadius: 17, backgroundColor: '#3D6AF2',  flexDirection: 'row',},
    title: {color: '#fff'},
    imageLeft: {height: 40, width: 40},
    imageRight: {height: 40, width: 40}
}
