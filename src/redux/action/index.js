export const ActionTypes = {
    TEST_REDUCER: 'TEST_REDUCER',
    SET_LANGUAGE: 'SET_LANGUAGE',
};

export const actions = {
    testReducer: function() {
        return {
            type: ActionTypes.TEST_REDUCER
        }
    },
    setLanguage: function (language) {
        return {
            type: ActionTypes.SET_LANGUAGE,
            payload: language
        }
    },
};