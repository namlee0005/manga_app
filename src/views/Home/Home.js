import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { actions } from '../../redux/action';
import { useDispatch, useSelector } from 'react-redux';
import { Styles, Ultils } from '../../config/Ultils';
import Swiper from 'react-native-swiper';
import FastImage from 'react-native-fast-image';

const images = [
    require('../../assets/images/img1.jpeg'),
    require('../../assets/images/img2.jpg'),
    require('../../assets/images/img3.jpg'),
    require('../../assets/images/img2.jpg'),
    require('../../assets/images/img3.jpg'),
]

const Home = ({ navigation }) => {

    return (
        <View style={Styles.container}>
            <View style={styles.wrapper}>
                <Swiper
                    showsButtons={true}
                    autoplay={true}
                    dotStyle={{backgroundColor:'rgba(255, 255, 255, .7)', width: 6, height: 6,}}
                    activeDotStyle={{backgroundColor:'rgba(243, 22, 22, .7)', width: 7, height: 7,}}
                    nextButton={<View></View>}
                    prevButton={<View></View>}
                >
                    {
                        images.map((item) => {
                            return (
                                <FastImage 
                                source={item} 
                                style={{ height: Ultils.dimensions.height/4, width: Ultils.dimensions.width }} 
                                resizeMode={FastImage.resizeMode.cover}
                                />
                            )
                        })
                    }

                </Swiper>
            </View>
            {/* <View style={{backgroundColor: "red", flex:1, marginTop: -20, borderTopLeftRadius: 10, borderTopRightRadius: 10}}>

            </View> */}

        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: { height: Ultils.dimensions.height / 4 },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB'
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    }
});

export default Home
