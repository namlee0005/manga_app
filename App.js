import React, { Component, useEffect, useRef } from 'react';
import { sagaMiddleware, store } from './src/redux/store';
import RootContainer from './src/navigation/rootScreen';
import rootSaga from './src/redux/sagas';
import { Provider } from 'react-redux';
import * as NavigationService from './src/navigation/navigateService';
// import CodePush from 'react-native-code-push'
sagaMiddleware.run(rootSaga);

const App = () => {
    console.disableYellowBox = true;

    const navigator = useRef()

    useEffect(() => {
        NavigationService.setNavigator(navigator)
    });

    return (
        <Provider store={store}>
            <RootContainer ref={navigator} />
        </Provider>
    )
}
export default App;
