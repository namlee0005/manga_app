import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { actions as actionsApp} from '../../redux/action'

export class Login extends Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <TouchableOpacity onPress={() => this.props.onTestReducer()}>
                    <Text> Introductions </Text>
                </TouchableOpacity>

            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = dispatch => ({
    onTestReducer: () => dispatch(actionsApp.testReducer())
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
