import { createStackNavigator } from '@react-navigation/stack';
import Home from '../../views/Home/Home';
import React from 'react';
const Stack = createStackNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator headerMode={'none'}>
            <Stack.Screen name="Home" component={Home} />
        </Stack.Navigator>
    );
}

export default HomeStack;
