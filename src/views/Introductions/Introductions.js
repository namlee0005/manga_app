import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native'
import {actions} from '../../redux/action'
import {useDispatch, useSelector} from 'react-redux';
const Introductions = ({ navigation }) => {

    const dispatch = useDispatch();
    const testReducer = () => dispatch(actions.testReducer());

    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center"}}>
            <TouchableOpacity onPress={() => testReducer()}>
                <Text> Introductions </Text>
            </TouchableOpacity>

        </View>
    )
}

export default Introductions
