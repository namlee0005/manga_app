import 'react-native-gesture-handler';
import React, { useEffect, useState, useRef, useImperativeHandle } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack'
import { View, Image, NativeModules } from 'react-native';
import { CONST } from '../../constant';
import AuthStack from '../stack/AuthStack';
import MainDrawer from '../drawer';
import { actions as actionApp } from '../../redux/action';
import { connect } from 'react-redux';
import Introductions from '../../views/Introductions/Introductions';
import { Ultils } from '../../config/Ultils'
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux'
// import Media from '../../Media';

const AuthLoading = () => {

    const navigation = useNavigation();
    const dispatch = useDispatch();
    
    useEffect(() => {
        async function setLocale() {
            let locale = await AsyncStorage.getItem('LANGUAGE');
            if (locale) {
                dispatch(actionApp.setLanguage(locale))
                return;
            }
            if (
                NativeModules.SettingsManager &&
                NativeModules.SettingsManager.settings &&
                NativeModules.SettingsManager.settings.AppleLanguages
            ) {
                locale = NativeModules.SettingsManager.settings.AppleLanguages[0];
                // Android
            } else if (NativeModules.I18nManager) {
                locale = NativeModules.I18nManager.localeIdentifier.slice(0,2);
                dispatch(actionApp.setLanguage(locale))
            }

            if (locale === undefined) {
                await AsyncStorage.setItem('LANGUAGE', 'vi');
                dispatch(actionApp.setLanguage(locale))
                return
            }
            await AsyncStorage.setItem('LANGUAGE', locale)
            dispatch(actionApp.setLanguage(locale))
        }
        // async function checkApp() {
        //     const token = await AsyncStorage.getItem(CONST.TOKEN);
        //     const intro = await AsyncStorage.getItem(CONST.INTRO);
        //     if (intro) {
        //         if (token) {
        //             navigation.navigate('MainApp');
        //         } else {
        //             navigation.navigate('AuthStack');
        //         }
        //     } else {
        //         navigation.navigate('Introductions');
        //     }
        // }
        navigation.navigate('MainApp');
        // checkApp();
        setLocale();
    })

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Image
                // source={Media.LogoApp}
                style={{ tintColor: '#c3333c', width: Ultils.dimensions.width / 4, height: Ultils.dimensions.width / 4 }}
                resizeMode={'contain'}
            />
        </View>
    )

}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = dispatch => ({

});
const Loading = connect(mapStateToProps, mapDispatchToProps)(AuthLoading);

const Stack = createStackNavigator();


const RootContainer = React.forwardRef((props, ref) => {

    const navigationRef = useRef();
    useImperativeHandle(ref, () => navigationRef.current);

    return (
        <NavigationContainer ref={navigationRef}>
            <Stack.Navigator initialRouteName="AuthLoading" headerMode={"none"}>
                <Stack.Screen
                    name="AuthLoading"
                    component={Loading}
                    options={null} />
                <Stack.Screen
                    name="Introductions"
                    component={Introductions}
                />
                <Stack.Screen name="AuthStack" component={AuthStack} />
                <Stack.Screen name="MainApp" component={MainDrawer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
})

export default RootContainer;
