import { Dimensions, Platform, StyleSheet } from 'react-native';
import { CONST } from '../constant';
import moment from 'moment';
import language from '../locales'

export const Ultils = {
    dimensions: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    ios: Platform.OS === 'ios',
    convertTime: (time) => moment(time).fromNow(true),
    converDateTime: (time) => moment(time).format('DD-MM-YYYY HH:ss'),
    converDate: (time) => moment(time).format('DD-MM-YYYY'),
    translate(locale, screen, key) {
        const cache = language.cache
        if (cache && cache[locale] && cache[locale][screen] && cache[locale][screen][key] && cache[locale][screen][key] !== "") {
            return cache[locale][screen][key];
        }
        return key
    },
};

export const Styles = StyleSheet.create({
    container: { flex: 1 },
    center: { alignItems: "center", justifyContent: "center" },
    rowCenterBetween: { flexDirection: "row", alignItems: "center", justifyContent: "space-between" },
    rowCenterAround: { flexDirection: "row", alignItems: "center", justifyContent: "space-around" },
    rowCenter: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    dividerVertical: {
        backgroundColor: '#CCCCCC',
        width: 1,
        height: 20,
        marginLeft: '4%',
        borderRadius: 1.5
    },
    dividerHorizontal: {
        backgroundColor: '#CCCCCC',
        height: 1,
        borderRadius: 1.5,
        marginTop: Platform.OS === 'ios' ? 3 : 0
    },
});

export const Media = {
    
};
