import { ActionTypes } from '../action';

const DEFAULT_STATE = {
    test: [],
    locale: '',
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        // case ActionTypes.TEST_REDUCER:
        //     console.log('tesst')
        //     return {
        //         ...state,
        //     };
        case ActionTypes.SET_LANGUAGE:
            return {
                ...state,
                locale: action.payload,
            };
        default:
            return state;
    }
}